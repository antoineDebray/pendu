import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.layout.Region;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.ButtonBar.ButtonData ;

import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("/usr/share/dict/french", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.panelCentral= new BorderPane();
        this.chargerImages("./img");
        ImageView img1= new ImageView("file:./img/home.png");
        ImageView img2= new ImageView("file:./img/parametres.png");
        img2.setFitWidth(24);
        img1.setFitWidth(24);
        img2.setPreserveRatio(true);
        img1.setPreserveRatio(true);
        this.boutonParametres=new Button("", img2);
        this.boutonMaison= new Button("",img1);
        // this.btn1.setOnAction(new RetourAccueil(this.modelePendu,this));
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 800, 1000);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre(){
        // A implementer          
        BorderPane banniere = new BorderPane();
        Text titre= new Text("Jeu du pendu");
        titre.setFont(Font.font (" Arial" , FontWeight.BOLD , 32) ) ;
        banniere.setLeft(titre);
        HBox boutons= new HBox();
        ImageView img3=new ImageView("file:./img/info.png");
        img3.setFitWidth(24);
        img3.setPreserveRatio(true);
        Button bouton3= new Button("",img3);
        boutons.getChildren().addAll(this.boutonMaison, this.boutonParametres,bouton3);
        banniere.setRight(boutons);
        banniere.setBackground(new Background(new BackgroundFill(Color.rgb(230, 230, 250), new CornerRadii(0), Insets.EMPTY)));
        banniere.setPadding(new Insets(10,10,10,10));
        return banniere;
    }

    // /**
     // * @return le panel du chronomètre
     // */
    // private TitledPane leChrono(){
        // A implementer
        // TitledPane res = new TitledPane();
        // return res;
    // }

    // /**
     // * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     // *         de progression et le clavier
     // */
    // private Pane fenetreJeu(){
        // A implementer
        // Pane res = new Pane();
        // return res;
    // }

    // /**
     // * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     // */
    // private Pane fenetreAccueil(){
        // A implementer    
        // Pane res = new Pane();
        // return res;
    // }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil(){
        VBox box= new VBox();
        Button btn=new Button("Lancer une partie");
        RadioButton facile= new RadioButton("facile");
        RadioButton medium= new RadioButton("Médium");
        RadioButton difficile= new RadioButton("Difficile");
        RadioButton expert =new RadioButton("Expert");
        ToggleGroup to= new ToggleGroup();
        VBox box2= new VBox();
        ControleurNiveau controle= new ControleurNiveau(this.modelePendu);
        facile.setToggleGroup(to);
        medium.setToggleGroup(to);
        difficile.setToggleGroup(to);
        expert.setToggleGroup(to);
        if(this.modelePendu.getNiveau()==0){
            facile.setSelected(true);
        }
        if(this.modelePendu.getNiveau()==2){
            medium.setSelected(true);
        }
        if(this.modelePendu.getNiveau()==3){
            difficile.setSelected(true);
        }
        if(this.modelePendu.getNiveau()==4){
            expert.setSelected(true);
        }
        facile.setSelected(true);
        box2.getChildren().addAll(facile,medium,difficile,expert);
        box2.setPadding(new Insets(10,50,10,10));
        TitledPane ti= new TitledPane("niveau de difficulté",box2);
        ti.setCollapsible(false);
        box.getChildren().addAll(btn,ti);
        box.setPadding(new Insets(10,50,10,10));
        box.setSpacing(30);       
        this.panelCentral.setCenter(box);
        
    }
    
    public void modeJeu(){
        BorderPane border= new BorderPane();
        VBox center= new VBox();
        VBox right= new VBox();
        this.motCrypte= new Text(this.modelePendu.getMotCrypte());
        this.dessin= new ImageView("file:./img/pendu0.png");
        this.pg= new ProgressBar(0);
        this.clavier= new Clavier("abcdefghijklomopqrstuvwxyz", null);
        
        center.getChildren().addAll(this.motCrypte, this.dessin, this.pg, this.clavier);
        border.setCenter(center);
        this.panelCentral.setCenter(border);
    }
    
    public void modeParametres(){
        // A implémenter
    }

    /** lance une partie */
    public void lancePartie(){
        // A implementer
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        // A implementer
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        // A implémenter
        return null; // A enlever
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);        
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        // this.modeAccueil();
        this.modeJeu();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}
